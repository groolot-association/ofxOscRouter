// =============================================================================
//
// Copyright (c) 2009-2015 Christopher Baker <http://christopherbaker.net>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// =============================================================================

#pragma once

#include "osc_match.h"
#include "Poco/RegularExpression.h"
#include "Poco/String.h"

#include "ofxOsc.h"
#include "ofxOscRouterBaseNode.h"

typedef std::function<void ()> NoArgPlugFunc;
typedef std::function<void (const ofxOscMessage& m)> OscMessagePlugFunc;

//extern "C" {
#include "osc_match.h"
//}

class ofxOscRouterBaseNode
{
public:

    // TODO: automaticaly add and remove slashes as needed
    // TODO: Pattern matching with methods
    // http://opensoundcontrol.org/spec-1_0-examples#OSCstrings
    // Order of Invocation of OSC Methods matched
    //  by OSC Messages in an OSC Bundle
    ofxOscRouterBaseNode();

    //ofxOscRouterBaseNode(ofxOscRouterBaseNode* _oscParent, std::string _oscNodeName);

    virtual ~ofxOscRouterBaseNode();

    // the address here is the remaining bit of the address, as it is processed and pruned
    virtual void processOscCommand(const std::string& command, const ofxOscMessage& m) = 0;

    void routeOscMessage(const std::string& pattern, ofxOscMessage& m);

    // recursively locate the root node
    ofxOscRouterBaseNode* getOscRoot();

    virtual std::set<std::string>& getOscNodeAliases() = 0;
    virtual const std::set<std::string>& getOscNodeAliases() const = 0;

    virtual std::string getFirstOscNodeAlias() const;
    virtual std::string getLastOscNodeAlias() const;
    bool hasOscNodeAlias(const std::string& alias) const;

    // parent directory
    bool hasParents() const;
    const ofxOscRouterBaseNode* getFirstOscParent() const;
    ofxOscRouterBaseNode* getFirstOscParent();
    const ofxOscRouterBaseNode* getLastOscParent() const;
    ofxOscRouterBaseNode* getLastOscParent();

    bool hasOscParent(ofxOscRouterBaseNode* _oscParent) const;
    bool addOscParent(ofxOscRouterBaseNode* _oscParent);
    bool removeOscParent(ofxOscRouterBaseNode* _oscParent);

    // child directory
    bool hasChildren() const;
    bool hasChildWithAlias(const std::string& alias) const;
    bool hasChildWithAlias(const std::set<std::string>& aliases) const;
    bool hasChildWithAlias(const std::set<std::string>& aliases, std::string& clashName) const;

    ofxOscRouterBaseNode* getFirstOscChild();
    const ofxOscRouterBaseNode* getFirstOscChild() const;

    std::set<ofxOscRouterBaseNode*>& getOscChildren();
    const std::set<ofxOscRouterBaseNode*>& getOscChildren() const;

    bool hasOscChild(ofxOscRouterBaseNode* oscChild) const;
    bool addOscChild(ofxOscRouterBaseNode* oscChild);
    bool removeOscChild(ofxOscRouterBaseNode* oscChild);

    // method directory
    std::set<std::string>& getOscMethods();
    const std::set<std::string>& getOscMethods() const;

    bool hasOscMethod(const std::string& _method) const;
    bool addOscMethod(const std::string& _method);
    bool removeOscMethod(const std::string& _method);

    bool isNodeActive() const;
    void setNodeActive(bool _bNodeEnabled);


    std::string getFirstOscPath() const;
    std::string getLastOscPath() const;



    //    bool hasOscPlugMethod(std::string _method);
    //    bool addOscPlugMethod(std::string _method, NoArgPlugFunc func);
    //    bool addOscPlugMethod(std::string _method, OscMessagePlugFunc func);
    //    bool removeOscPlugMethod(std::string _method);
    //    bool executeOscPlugMethod(std::string _method, const ofxOscMessage& m);


    // TODO: add OSC Methods that link directly to a method, rather than go
    // through the processOscCommand callback.

    std::string schemaToString(std::uint8_t _level = 0) const
    {
        uint8_t tabSize = 3;
        std::stringstream ss;
        std::set<std::string>::iterator stringIter;
        std::set<std::string> aliases = getOscNodeAliases();

        for(stringIter = aliases.begin(); stringIter != aliases.end(); stringIter++)
        {
            ss << std::string(tabSize * _level, ' ') << "/" << *stringIter << "/" << std::endl;
        }

        std::set<std::string> methods  = getOscMethods();

        for(stringIter = methods.begin(); stringIter != methods.end(); stringIter++)
        {
            ss << std::string(tabSize * (_level + 1), ' ') << *stringIter << std::endl;
        }

        std::set<ofxOscRouterBaseNode*>::iterator childIter;
        std::set<ofxOscRouterBaseNode*> children = getOscChildren();

        for(childIter = children.begin(); childIter != children.end(); childIter++)
        {
            ss << (*childIter)->schemaToString(_level + 1);
        }

        return ss.str();
    };

    // utility methods
    static std::string normalizeOscNodeName(const std::string& name);
    static bool   isValidOscNodeName(const std::string& name);
    static std::string normalizeOscCommand(const std::string& name);
    static bool   isValidOscCommand(const std::string& name);

    static bool isMatch(const std::string& s0, const std::string& s1);
    static bool validateOscSignature(const std::string&, const ofxOscMessage& m);

    static ofColor getArgsAsColor(const ofxOscMessage& m, int index, int endIndex);
    static ofPoint getArgsAsPoint(const ofxOscMessage& m, int index, int endIndex);

    static ofColor getArgsAsColor(const ofxOscMessage& m, int index);
    static ofPoint getArgsAsPoint(const ofxOscMessage& m, int index);

    static bool   getArgAsBoolUnchecked(const ofxOscMessage& m, int index);
    static float  getArgAsFloatUnchecked(const ofxOscMessage& m, int index);
    static int    getArgAsIntUnchecked(const ofxOscMessage& m, int index);
    static std::string getArgAsStringUnchecked(const ofxOscMessage& m, int index);

    static std::vector<bool>   getArgsAsBoolArray(const ofxOscMessage& m, int index, int endIndex);
    static std::vector<float>  getArgsAsFloatArray(const ofxOscMessage& m, int index, int endIndex);
    static std::vector<int>    getArgsAsIntArray(const ofxOscMessage& m, int index, int endIndex);
    static std::vector<std::string> getArgsAsStringArray(const ofxOscMessage& m, int index, int endIndex);

    static std::vector<bool>   getArgsAsBoolArray(const ofxOscMessage& m, int index);
    static std::vector<float>  getArgsAsFloatArray(const ofxOscMessage& m, int index);
    static std::vector<int>    getArgsAsIntArray(const ofxOscMessage& m, int index);
    static std::vector<std::string> getArgsAsStringArray(const ofxOscMessage& m, int index);

    static std::string getMessageAsString(const ofxOscMessage& m);

protected:

    bool bNodeActive;

private:

    // these must be manipulated via methods (to prevent dangling links)
    std::set<std::string> oscMethods;

    unordered_map<std::string, NoArgPlugFunc>            noArgPlugFuncMap;
    unordered_map<std::string, NoArgPlugFunc>::iterator  noArgPlugFuncMapIter;

    unordered_map<std::string, OscMessagePlugFunc>            oscMessagePlugFuncMap;
    unordered_map<std::string, OscMessagePlugFunc>::iterator  oscMessagePlugFuncMapIter;

    std::set<ofxOscRouterBaseNode*>::iterator nodeIter;
    std::set<ofxOscRouterBaseNode*> oscParents;
    std::set<ofxOscRouterBaseNode*> oscChildren;

};
